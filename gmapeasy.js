Drupal.behaviors.gmapeasyBehavior = function (context) {
  var map;
  var infowindow;

  var lat = Drupal.settings.gmapeasy.lat;
  var long = Drupal.settings.gmapeasy.long;
  var zoom = Drupal.settings.gmapeasy.zoom;
  var text = Drupal.settings.gmapeasy.text;
  var width = Drupal.settings.gmapeasy.width;
  var height = Drupal.settings.gmapeasy.height;
  
  function initialize() {
    $("#gmapeasy-map").width(width);
    $("#gmapeasy-map").height(height);
    
    var position = new google.maps.LatLng(lat, long);

    map = new google.maps.Map(document.getElementById('gmapeasy-map'), {
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      center: position,
      zoom: zoom
    });

    var infowindow = new google.maps.InfoWindow({
      content: text
    });

    var marker = new google.maps.Marker({
      position: position,
      map: map,
      title: text
    });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map, marker);
    });
  }

  google.maps.event.addDomListener(window, 'load', initialize);
};
